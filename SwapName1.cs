﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameSwap
{
    class Program
    {
        static void Main(string[] args)
        {
            //Address user
           // Console.WriteLine("Hello and welcome to the name swap.\r\n");
            //Ask user to type first name
            Console.WriteLine("Please type your first name and push return\r\n");
            //Read name user input - call it fName for first 
            string fName = Console.ReadLine();
            //Show user we captured their answer and display fName within output from prog.
            Console.WriteLine("\r\nHi " + fName + ". Now enter your last name and press return\r\n");
            string lName = Console.ReadLine();
            Console.WriteLine("\r\nYou have entered " + fName + " " + lName + ", \r\n");
            //fullName uses first input combined with second - add space for literary purpose. 
            string fullName = (fName + " " + lName);
            //SwapName - string holding last , first - with spacing between both strings
            string swapName = (lName + ", " + fName);

            /*here is another method using split
            string[] names = fullName.Split(' ');
             string firstName = names.First();
            string lastName = names.Last();*/

            //SwapName - address user showing them we will swap their input
            Console.WriteLine("Now let's show your name with both the first and last names swapping\r\n ");
            //Tell user SwapName worked and show result
            Console.WriteLine("Your name last and first is : \r\n" + swapName + ".\r\n");
            //Ask user if we should take SwapName back to FullName
            Console.WriteLine("Should we now reverse " + swapName + " back to the way you entered it?\r\nType: yes/no and hit return.");
            //Create string answer to collect user input,  their decision decides output
            string answer = Console.ReadLine();
            // if statement using invariant culture ignore case to allow answers of
            // yes, Yes, y, Y, yeah, yeah etc.
            if (answer.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
            {
                //if ans is comparable to yes - tell user they chose 'y' - use fullname here
                Console.WriteLine("\r\nYou answered Yes! Your name is " + fullName + ".\r\n");
            }
            //else if is used for no answer - we use culture ignore case again to catch
            //all answers close to no - N, n, no, nope, etc.
            else if (answer.Equals("no", StringComparison.InvariantCultureIgnoreCase))
            {
                //else if is triggered by no answer - we give this output to user
                //displaying their input 
                Console.WriteLine("You answered no.");
            }
            //tell user thank you for using NameSwap Class
            Console.WriteLine("Thank you for using the Name Swap " + fullName + "!");
            //I used this becuase i still don't know how to keep the terminal window open 
            //after completion. Advice on this minor item would be great!
            Console.ReadLine();
        }
    }
}

            //Thanks for any input

            /*------------------------------TESTING ------------------------------------*/
            /*Test 1 - 
             input - Trevor
             input - Hain
             Output - Trevor Hain  
             Swap -
             Output - Hain, Trevor
             yes - output Trevor Hain
             output - Trevor Hain
             
             Test 2 -
             in Mickey
               in Mouse 
               out -Hi mickey
               out Mickey Mouse
               out Mouse, Mickey
               in -N
               out -Mickey Mouse

               test 3
               in -Don Juan
               out -Don Juan
               in -Demarco
               out -Don Juan Demarco
               swap - out -Demarco, Don Juan
               in -yeah
               out -Don Juan Demarco

                All testing works - One first name or two
                */




    