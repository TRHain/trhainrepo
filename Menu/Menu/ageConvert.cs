﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ageConvert
{
    class AgeConvert
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your birthday like so: (mm/dd/year) and push return");
            DateTime birth = DateTime.Parse(Console.ReadLine());
            //test value - my birthday
            //DateTime birth = DateTime.Parse("09/03/2007");

            //not using exact time (watch time) however it's precie to date
            DateTime today = DateTime.Today;

            //.net guarantees this is precise - call for timespan
            TimeSpan age = today - birth;

            //total number of days down to complete precision based off of time now
            double ageInDays = age.TotalDays;

            //statistical value for 400 years
            double daysInYear = 365.2425;

            //this is not as precise but verified as humanly preise
            double ageInYears = ageInDays / daysInYear;

            double ageInWeeks = ageInYears / 52;
            double ageInHours = ageInYears / 24;
            double ageInMinutes = ageInYears / 525949.2;

            double ageInSeconds = ageInYears / 31556952;

            Console.WriteLine("You are: \r\n" + ageInYears + " Years old."); Console.WriteLine(age.TotalDays + " Days old.");

            Console.WriteLine(age.TotalHours + " Hours old.");

            Console.WriteLine(age.TotalMinutes + " Minutes old.");

            Console.WriteLine(age.TotalSeconds + " Seconds old.");

            Console.ReadLine();




        }
    }
}
