﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempConvert
{
    class TempConvert
    {
        static void Main()
        {//Main
            //create Fahrenheit for input
            int Fahrenheit;
            //Create Celcius for input
            int Celcius;


            Console.WriteLine("Please enter a temperature in Celcius and hit return: ");
            Fahrenheit = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Please enter a temperature in Fahrenheit and hit return: ");
            Celcius = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("{0} degrees Celcius = {1} degrees Fahrenheit ", Fahrenheit, TempConvert.ConvCToF(Fahrenheit));
            Console.WriteLine("{0} degrees Fahrenheit = {1} degrees Celcius ", Celcius, TempConvert.ConvFToC(Celcius));
            //readline is here so to stop program
            Console.ReadLine();
        }
        //TempConvert Method held here
        static class TempConvert
        {//MEthod for conversion of celsius to fahrenheit
            public static double ConvCToF(double c) // create double c for return
            {//returns value of degrees fahrenheit
                //value found by simple equation of ((9/5)*user input for C) Add 32=
                return ((9.0 / 5.0) * c) + 32;
            }
            // convert f-c
            public static double ConvFToC(double f) //create double f for return
            {
                // returns value - method to get solution of conv from F to C
                //(5/9) * the user input of (F -32)
                return (5.0 / 9.0) * (f - 32);
            }
        }
    }
}


