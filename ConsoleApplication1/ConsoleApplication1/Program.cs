﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickANumberValPractice
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            /*This is a simple test using strings and doing simple validation
             we simply ask the user to pick a number between 1 and 10
             if they go above or below 1-10 then an if statement tells user that
             they did not choose between the requested numbers
             
             this is a simple test of code and is not safe for actual validation*/
            Console.Title = "*** Pick A Number, 1-10! ***";
            //Let's introduce the user
           
            //Ask user to enter name
            Console.WriteLine("Please enter your name and hit return\r\n");
            //call string 'name'
            string name = Console.ReadLine();
            //got info from user - reply showing their name
            Console.WriteLine("Hello, " +name+ " lets play a game!\r\n");
            //Ask the user for input
            Console.WriteLine("\r\n Lets see if you can read my mind!\r\nThe number is completely random\r\n \r\nPlease enter a number between 1 and 10 and hit return");
            // make sure we parse string to int - getting string from user
            int number = int.Parse (Console.ReadLine());

            //give user error message if they go over 10 
            if (number > 10)
            {
                //rite to user - should be less
                Console.WriteLine("Hey, the number should be 10 or less!\r\n");
            }
            //if statement for lower than 0
            if (number < 0)
            {
                //write to user - should be more
                Console.WriteLine("Hey, number should be 0 or more! \r\n");
            }
            //else statement ends loop - congratulate user
            else
                Console.WriteLine("Good job!\r\n \r\n");
            //Thank user 
            Console.WriteLine("Thank you very kindly for playing today!");
            //done

               
        }
    }
}
