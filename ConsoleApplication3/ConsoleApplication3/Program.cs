﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backwards
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello and thank you for visiting today.\r\nToday, let's take a look at strings. \r\n");
            Console.WriteLine("Please enter a sentence containing 6 or more words\r\n(please separate the 6+ words with spaces or other)\r\nWhen done,please press enter.");

            char[] separatingChars = { ' ', ',', '.', ':', ';', '-', '_', };

            string text = Console.ReadLine();

            Console.WriteLine("Your Text: {0}", text);

            string[] words = text.Split(separatingChars);

            Console.WriteLine("{0} words in text:", words.Length);

            /*foreach (string s in words)
            {
                Console.WriteLine(s);
            }*/

            Console.WriteLine("Thank you for playing today.\r\nI hope you have learned something today\r\nPress any key to escape");

            string b = text;

            string[] a = b.Split(' ');


            Array.Reverse(a);

            Console.WriteLine("The reverse of your string is:");

            for (int i = 0; i <= a.Length - 1; i++)
            {
                Console.WriteLine(a[i] + " " + ' ');
            }
           
            // Console.WriteLine("Thank You");
            Console.ReadLine();
        }
    }
}

  